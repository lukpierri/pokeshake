# PokeShake(speare)

Would you ever want to describe your pokemon in a brand new different way? let's try PokeShake API!


## Dev setup

* Install miniconda for your OS distribution. Follow instructions at: [https://docs.conda.io/projects/conda/en/latest/user-guide/install/](https://docs.conda.io/projects/conda/en/latest/user-guide/install/)
* Create a virtualenv via <code>conda create --name pokeshake</code>
* Activate the virtualenv <code>conda activate pokeshake</code>
* Install pip3 via <code>conda install pip</code>
* Install requirements via <code>pip3 install -r requirements.txt</code>

#### Versioning
The pokeshake project is managed by git versioning system. 

Develop branch is used for developing new features.
Master branch is used for release tagging.

## Steps to run the app locally:

1. <code>cd app/</code>
2. <code>export FLASK_APP=app.py>
3. Run <code>python3 -m flask app.py</code>. it will deploy the flask application at localhost:5000. see <code>python3 -m flask --help</code> for further details.
4. Access the app via your browser: [http://localhost:5000](http://localhost:5000)

## Steps to run the app inside Docker container(s)

1. Install docker for your OS distribution. Follow instructions at: [https://docs.docker.com/get-docker/](https://docs.conda.io/projects/conda/en/latest/user-guide/install/)
2. Install docker-compose for your OS distribution. Follow instructions at: [https://docs.docker.com/compose/install/](https://docs.docker.com/compose/install/)
1. Build containers locally: `docker-compose -f docker-compose.yml build`
2. Run containers locally: `docker-compose -f docker-compose.yml up`
3. Turn down containers: ` docker-compose -f docker-compose.yml down`

#### Architecture
    
docker-compose helps in simulating a real production environment.

<p align="center">
    <img src="img/architecture.png" width=75%>
</p>

As you can see in the above picture, http requests are handled by the web-server Nginx, which forward them to the Flask pokeshake service, exposed via uwsgi.


## Configuration
#### App (config.ini)

* [urls]: contains pokeapi and shakespearean translator urls
* [cache]: establishes the cache type in pokeshake api. Only NAIVE (simple memory cache) is available at the moment
* [mock]: allows to specify if the translator service should be mocked. If 'yes' is specified, an identity (text -> text) service will answer to the user requests
* [templates]: specifies the templates folder for the Flask app
* [local_server]: contains host, port and debug mode for the local server 

#### uWSGI (uwsgi.ini)
Relevant uSGI configuration properties are below:

* http-socket (host:port): allows to specify the host and port where the Flask application should be deployed. Changes in this property should be reflected also in Nginx configuration file and docker-compose.yml (see below)
* stats: specifies the address containing some statistics of the uwsgi application server. It can be queried via telnet (<code>telnet host port</code>)

#### Nginx (pokeshake-nginx.conf)
Relevant Nginx configuration properties are below:

* listen: Nginx listen port. Remember to expose it in its service in docker-compose
* location: path of the app. In this case, the application is exposed at root level (/)
* uwsgi_pass: address of the uwsgi service. It this case, pokeshake:8080 is built by the container name followed the port where whe want the service

## Testing:

`pytest` is ran from within the `/app` directory inside the container on build. The container build will not be successful if a test fails. 
The following screenshot helps to configure testing in pycharm:


<p align="center">
  <img src="img/pycharm-test-run-configuration.png" width=75%>
</p>



## Api Documentation:

<details><summary><b>Sample API request: </b><code>GET /pokemon/&#60;string:pokemon-name&#62;</code></summary>
<p>

This an endpoint of the api.

**Endpoint**: `GET /pokemon/<string:pokemon-name>`

**Return constraints:**

```json
{
    "name": String, 
    "description": String
}
```

**Return example:**

```json
{
    "name": "bulbasaur", 
    "description": "A strange seed wast planted on its back at birth. The plant sprouts and grows with this pokémon."
}
```

</p>
</details>

## Swagger
At http://{host}:{port}/swagger/ will be deployed a very little GUI for playing with pokeshake api.
See [https://swagger.io/](https://swagger.io/) for further details.

## Future developments

A couple of new functionalities are meant to be developed in the future:

1. Redis cache, useful to manage much more key/value pairs.
2. Additional api route, which allows to specify the Shakespearean translator API key. Pokeshake is currently using the public api, which allows to perform 60 request per day / 5 requests per hour. By implementing this new route, we will allow users who want to pay for a key to use it with pokeshake api.