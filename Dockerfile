FROM python:3.7

RUN apt update
RUN apt install -y --no-install-recommends \
        libatlas-base-dev gfortran uwsgi-plugin-python3

RUN pip3 install uwsgi
RUN pip3 wheel --wheel-dir /root/wheels psycopg2
RUN pip3 wheel --wheel-dir /root/wheels cryptography

RUN mkdir -p /var/www/pokeshake/app

COPY ./requirements.txt /var/www/pokeshake/app/requirements.txt

COPY scripts/deploy.sh /var/www/pokeshake/app/deploy.sh

RUN pip3 install -r /var/www/pokeshake/app/requirements.txt

COPY server-conf/uwsgi.ini /var/www/pokeshake/app/

COPY /app /var/www/pokeshake/app


RUN useradd --no-create-home flask

WORKDIR /var/www/pokeshake/app

RUN pytest

CMD ["./deploy.sh"]


