# Test the api endpoints of the flask app

import pytest

from app.app import app


@pytest.fixture(scope='module')
def test_client():
    """This fixture will have the test_client() of the flask app stored and ready to be used by the other test cases.
    scope='module' will only spin it up once for all test cases and re-use the flask test_client. This is helpful for
    sessions and other settings."""
    app.config['TESTING'] = True
    testing_client = app.test_client()
    ctx = app.app_context()
    ctx.push()

    yield testing_client

    ctx.pop()


def test_home_page(test_client):
    """Test the root directory"""
    response = test_client.get('/')
    assert response.mimetype == 'text/html'
    assert response.status_code == 200


def test_api_pokeshake(test_client):
    """Test api variable"""
    # response = test_client.get('/pokemon/charizard')
    # assert response.mimetype == 'application/json'
    # assert response.status_code == 200
    # assert response.json == {"name": "charizard", "description": "Spits fire that\nis hot enough to\nmelt boulders.\x0cKnown to cause\nforest fires\nunintentionally."}
