from typing import List

import pytest

from service.cache import MemoryCache, Cache


@pytest.fixture('function')
def cache():
    cache = MemoryCache()
    yield cache


def test_empty_get(cache: Cache):
    assert cache.get('test_key') is None


def test_get(cache: Cache):
    print(cache.put('key', 'value'))
    assert cache.get('key') == 'value'


def test_empty_put(cache: Cache):
    assert cache.put('key', 'value') is None  # old value not present


def test_overwrite_put(cache: Cache):
    assert cache.put('key', 'value') is None
    assert cache.put('key', 'value') is 'value'


def test_invalid_put(cache: Cache):
    with pytest.raises(ValueError):
        cache.put(None, None)

    with pytest.raises(ValueError):
        cache.put('key', None)

    with pytest.raises(ValueError):
        cache.put(None, 'value')


def test_invalid_get(cache: Cache):
    with pytest.raises(ValueError):
        cache.get(None)
