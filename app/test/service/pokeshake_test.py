import pytest

from service.cache import Cache
from service.utils import InternalErrorException

import app.service.pokeshake as pk


@pytest.fixture(scope='module')
def pokeshake():
    pokeshake = pk.PokeShake(Cache.create_cache('NAIVE'), 'pokemon_url', 'translator_url', mock_translator=True)
    yield pokeshake


def test_retrieve_pokemon_description_ok(pokeshake: pk.PokeShake):
    descriptions = [{'flavor_text': 'test description1', 'language': {'name': 'fr'}},
                    {'flavor_text': 'test description2', 'language': {'name': 'en'}}]
    pokemon = dict(flavor_text_entries=descriptions)

    assert pokeshake.retrieve_pokemon_description(pokemon, 'test_pk') == 'test description2'


def test_retrieve_pokemon_description_english_not_found(pokeshake: pk.PokeShake):
    descriptions = [{'flavor_text': 'test description1', 'language': {'name': 'fr'}},
                    {'flavor_text': 'test description2', 'language': {'name': 'it'}}]
    pokemon = dict(flavor_text_entries=descriptions)

    with pytest.raises(InternalErrorException):
        pokeshake.retrieve_pokemon_description(pokemon, 'test_pk')
