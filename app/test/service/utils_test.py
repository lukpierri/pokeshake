import pytest
import requests

from app.service.utils import handle_response, InternalErrorException


def test_handle_response():
    @handle_response
    def test_function_ok() -> requests.Response:
        test_response = requests.Response()
        test_response.status_code = 200
        test_response.raw = 'test_result'
        return test_response

    @handle_response
    def test_function_ko() -> requests.Response:
        test_response = requests.Response()
        test_response.status_code = 429
        return test_response

    with pytest.raises(InternalErrorException):
        test_function_ko()

    assert test_function_ok().raw == 'test_result'
