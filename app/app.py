import sys

from flask import Flask
from flask_swagger_ui import get_swaggerui_blueprint

import service.pokeshake as ps
from service.cache import Cache
from service.log import setup_custom_logger
from service.utils import load_config

logger = setup_custom_logger(__name__)

# load configuration

try:
    configuration = load_config('config.ini')
except Exception as e:
    logger.error(e)
    sys.exit(1)

# set up services

cache = Cache.create_cache(configuration['cache']['type'])

pokeshake = ps.PokeShake(cache, configuration['urls']['pokeapi'], configuration['urls']['translator'],
                         mock_translator=configuration['mock'].getboolean('translator'))

# set up flask up

logger.info("Setting up flask server")
app = Flask(__name__)

# main api routes
from rest.routes import main_route

logger.info("Registering api main route")
app.register_blueprint(main_route)

### swagger specific ###
SWAGGER_URL = '/swagger'
API_URL = '/static/swagger.json'
SWAGGERUI_BLUEPRINT = get_swaggerui_blueprint(
    SWAGGER_URL,
    API_URL,
    config={
        'app_name': "pokeshake"
    }
)

logger.info("Registering swagger route")
app.register_blueprint(SWAGGERUI_BLUEPRINT, url_prefix=SWAGGER_URL)