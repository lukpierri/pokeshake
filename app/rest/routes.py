from json import dumps

from flask import render_template, Response, Blueprint

from app.app import pokeshake, configuration
from service.log import setup_custom_logger
from service.utils import InternalErrorException

logger = setup_custom_logger(__name__)

main_route = Blueprint('main_route', __name__, template_folder=configuration['templates']['folder'])


@main_route.route('/', methods=['GET'])
def index():
    """Return the index.html page"""
    return render_template('index.html')


@main_route.route('/pokemon/<string:pokemon_name>', methods=['GET'])
def pokemon_description(pokemon_name: str) -> Response:
    """this is the main endpoint of the API. it takes a pokemon name as parameter
        and returns its shakespearean description.
    """
    logger.info("Received request to retrieve shakespearean description for %s", pokemon_name)
    try:
        description = pokeshake.pokeshake(pokemon_name)
        data = dict(name=pokemon_name, description=description)
    except InternalErrorException as e:
        logger.error(e.error_message)
        data = e.error_out()

    logger.debug("returning response body %s", data)
    return Response(dumps(data), status=200, mimetype='application/json')
