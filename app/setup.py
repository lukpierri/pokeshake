from setuptools import setup, find_packages

setup(name='pokeshake',
      version='0.1.2-SNAPSHOT',
      packages=find_packages(),
      author='Luca Pierri',
      author_email='lukepier95@gmail.com'
)
