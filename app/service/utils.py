import configparser
from functools import wraps
from typing import Callable

import requests

from service.log import setup_custom_logger

logger = setup_custom_logger(__name__)


class InternalErrorException(Exception):
    def __init__(self, error_code, error_message):
        self.error_code = error_code
        self.error_message = error_message

    def error_out(self):
        return make_error(self.error_code, self.error_message)


def make_error(status_code, message):
    response = {
        'status': status_code,
        'message': message,
    }
    logger.error('Returning error: %s', response)
    return response


def handle_response(request_fn: Callable[[str], requests.Response]):
    @wraps(request_fn)
    def decorated(*args, **kwargs):
        try:
            response = request_fn(*args, **kwargs)
        except requests.exceptions.Timeout:
            raise InternalErrorException(408, "Timeout contacting shakespeare translator service")

        status_code = response.status_code
        if status_code != 200:
            try:
                error = response.json()['error']['message']
            except Exception:
                if response.text:
                    error = response.text
                else:
                    error = 'Unable to perform http request'
            raise InternalErrorException(status_code, error)
        return response

    return decorated


def load_config(path: str) -> configparser.ConfigParser:
    config = configparser.ConfigParser()
    config.read(path)

    return config
