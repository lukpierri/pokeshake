import logging
import logging.config

logging.config.fileConfig('config.ini')

def setup_custom_logger(name):
    logger = logging.getLogger(name)
    return logger