from abc import ABC
from enum import Enum
from service.log import setup_custom_logger
from typing import TypeVar, Generic

logger = setup_custom_logger(__name__)

V = TypeVar('V')
K = TypeVar('K')


class Type(Enum):
    NAIVE = 1

    # TODO: other cache implementations
    def describe(self):
        # self is the member here

        return self.name, self.value

    def __str__(self):
        return '{0}'.format(self.name)


class Cache(Generic[K, V], ABC):
    def __init__(self):
        pass

    def put(self, key: K, value: V) -> V:
        pass

    def get(self, key: K) -> V:
        pass

    @staticmethod
    def create_cache(_type: str):
        if _type == str(Type.NAIVE):
            return MemoryCache()
        else:
            raise ValueError("Specified unsupported cache type")


class MemoryCache(Cache):

    def __init__(self):
        super().__init__()
        logger.info("Setting up cache")
        self.cache = dict()

    def put(self, key: K, value: V) -> V:
        """
            Try to put an element in the cache. If the key value pair is none, raises an error.
            If there is already an entry for the underlying key, overwrite its value and returns the previous value

            Returns the previous value (can be None if not initialized)
        """
        logger.debug("Caching value %s under key %s", value, key)
        if key and value:
            old_value = self.cache.get(key)
            if old_value and old_value != value:
                logger.debug("Overwriting previous cached value %s with new value %s", old_value, value)
            self.cache[key] = value

            return old_value
        else:
            raise ValueError("key value pair should be defined")

    def get(self, key: K) -> V:
        """
            Retrieve the value for the underlying key. Raises an error if the specified key is None
        """
        logger.debug("Retrieving value for key %s", key)
        if key:
            value = self.cache.get(key)
            logger.debug("Returning value %s for key %s", value, key)
            return value
        else:
            raise ValueError("key should be defined")
