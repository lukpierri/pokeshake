import requests

from service.cache import Cache
from service.log import setup_custom_logger
from service.utils import InternalErrorException, handle_response

logger = setup_custom_logger(__name__)


class PokeShake:

    def __init__(self, cache: Cache, pokemon_url: str, translator_url: str, mock_translator: bool = False):
        logger.debug("starting pokeshake service ...")
        self.cache = cache
        self.pokemon_url = pokemon_url
        self.translator_url = translator_url
        self.mock_translator = mock_translator

    def pokeshake(self, name: str) -> str:
        # lookup at cache
        cached_desc = self.cache.get(name)

        if cached_desc:
            return cached_desc

        # querying external service for pokemon details
        pokemon = self.get_pokemon(name)
        # extract english description from response
        description = self.retrieve_pokemon_description(pokemon, name)

        # translate in shakespearean english
        response = self.shakespearean_translation(description)
        # saving into cache
        self.cache.put(name, description)
        return response

    @staticmethod
    def retrieve_pokemon_description(pokemon: dict, name: str) -> str:
        descriptions = pokemon['flavor_text_entries']

        desired_desc = next(iter([desc['flavor_text'] for desc in descriptions if desc['language']['name'] == 'en']),
                            None)
        # TODO: handle broken encoding, i.e 'Shoots water at\nprey while in the\nwater.\x0cWithdraws into\nits shell when in\ndanger.'
        if not desired_desc:
            raise InternalErrorException(404, f"Cannot find english description pokemon {name}")

        return desired_desc

    def shakespearean_translation(self, text: str) -> str:
        logger.debug("translating text into shakespearean form ...")

        # useful for test, since with the public api you can do 60 request per day / 5 per hour
        if self.mock_translator:
            return text

        @handle_response
        def request_fn(t: str):
            return requests.post(
                self.translator_url,
                {'text': t}
            )

        translated = request_fn(text).json()['contents']['translated']
        return translated

    def get_pokemon(self, name: str) -> dict:
        logger.debug("querying description for pokemon %s", name)

        @handle_response
        def request_fn(n: str):
            return requests.get(
                f"{self.pokemon_url}/{n}"
            )

        response = request_fn(name)
        return response.json()
